import React, { useEffect, useState } from 'react';

// Import components
import Table from './Components/Table';
import Input from './Components/Input';
import SearchInput from './Components/SearchInput';

// import images
import github from './github-icon.svg'

const App = () => {

  // States
  const [nameList, setNameList] = useState(["Rose", "Mark"]);
  const [filteredNameList, setFilteredNameList] = useState([])
  const [filters, setFilters] = useState({ currentName: '', searchTerm: ''})
  const [alphabetized, setAlphabetized] = useState(false)

  // Function to execute when submit has been clicked
  const handleSubmitClick = () => {
    const nameFormat = /^[a-zA-Z]+\s?[a-zA-Z]+$/
    if (filters.currentName.match(nameFormat) && !nameList.includes(filters.currentName)){
      setNameList([ ...nameList, filters.currentName])
      setFilters({ ...filters, currentName: ''})
    } else if (nameList.includes(filters.currentName)){
      window.alert("Duplicate name!")
    } else {
      window.alert("Please enter valid name")
    }
  };

  // Function to track changes in currentName Value and set it accordingly
  const handleChange = (event) => {
    const value = event.target.value
    const newFilter = { ...filters, [event.target.name]: value }
    setFilters(newFilter)
  }

  // Sort nameList array alphabetically and set alphatized state as true
  const handleAlphabetize = () => {
    nameList.sort()
    setAlphabetized(true)
  }

  // UseEffect to track alphabetized state and to set as false when changed. Allows page to dynamically change when nameList is alphabetized
  useEffect(() => {
    setAlphabetized(false)
  }, [alphabetized])

  // Delete function allowing us to remove specific entry in array with confirmation required
  const handleDelete = (event) => {
    if (window.confirm(`Are you sure you want to delete ${event.target.id}'s name from employee list?`)) {
      setNameList(nameList.filter(name => name !== event.target.id))
    }
  }

  // Listening for updates on name list and filters and updating filteredNameList
  useEffect(() => {
    const regexSearch = new RegExp(filters.searchTerm, 'i')
    setFilteredNameList(nameList.filter((employee) => {
      return regexSearch.test(employee)
    }))
  }, [filters, nameList])



  return (
    <div>
      <header>
        <h1>HR3000</h1>
      </header>
      <h2>Welcome to the HR3000 Portal</h2>
      <main>
        <div id="employee-name-input">
          <p>Add New Employee:</p>
          <Input value={filters.currentName} handleChange={handleChange} />
          <button onClick={handleSubmitClick}>Add</button>
        </div>
        <p id="or">Or</p>
      <div id="search-term"> 
        <p>Search Employee:</p>
        <SearchInput handleChange={handleChange} filters={filters} />
      </div>
      <Table names={nameList} handleDelete={handleDelete} handleAlphabetize={handleAlphabetize} filters={filters} filteredNameList={filteredNameList}/>
      </main>
      <footer>
        <a  href="https://github.com/joinSamBalboa" target="_blank" rel="noreferrer"><img src={github} alt="github logo" /> Created by Jason Abimbola</a>
      </footer>
    </div>
  );
}

export default App;

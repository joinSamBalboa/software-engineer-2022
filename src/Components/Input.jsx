import React from 'react';

const Input = ({ handleChange, value }) => {
  return (
    <>
      <input value={value} name="currentName" type="text" placeholder="Employee Name" onInput={handleChange}/>
    </>
  )
};

export default Input;

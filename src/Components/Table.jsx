import React from 'react';



const Table = ({ names, handleDelete, handleAlphabetize, filters, filteredNameList }) => {
  return (
    <>
      <table id="employee-table">
      <div><button onClick={handleAlphabetize} >Sort A-Z</button></div>
        <thead>
          <tr>
          </tr>
        </thead>
        
        <tbody>
        {/* Mapping through array of names and returning in corrent format within table  */}
          { filteredNameList.length > 0 ?
            (filters.searchTerm !== '' ? filteredNameList : names).map(name => {
              return <tr key={name}>
              <td>{name}</td>
              <button onClick={handleDelete} id={name}>Remove</button>
            </tr>
          })
            :

            <p id="no-employees">No employees available</p>

          }

        </tbody>
      </table>
    </>
  )
};

export default Table;

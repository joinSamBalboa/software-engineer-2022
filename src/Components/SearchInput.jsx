import React from 'react';

const SearchInput = ({ handleChange, filters }) => {
  return (
    <>
      <input onChange={handleChange} data-testid="search" name="searchTerm" value={filters.searchTerm}/>
    </>
  )
};

export default SearchInput;
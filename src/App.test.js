import { render, screen } from '@testing-library/react';
import App from './App';

test('renders employees', () => {
  render(<App />);
  const linkElement = screen.getByText('Employees');
  expect(linkElement).toBeInTheDocument();
});
